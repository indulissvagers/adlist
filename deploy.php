<?php
namespace Deployer;

require 'recipe/symfony.php';

set('bin_dir', 'bin');

// Project name
set('application', 'adlist');

// Project repository
set('repository', 'git@bitbucket.org:indulissvagers/adlist.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server 
add('writable_dirs', []);


// Hosts

host('adlist.svagers.lv')
    ->user('svagers')
    ->port(22)
    ->stage('production')
    ->set('deploy_path', '~/apps/{{application}}');    
    
// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'database:migrate');