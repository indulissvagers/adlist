# Adlist

Adlist is a simple app for publishing advertisements.

### Deployment

Install dependencies

```
composer install --no-dev --optimize-autoloader
```
```
npm install
```

Clear and warmup cache
```
php bin/console cache:clear --env=prod --no-debug
```
```
./bin/console cache:warmup
```

Migrate database
```
./bin/console doctrine:migrations:migrate
```

Compile assets
```
./node_modules/.bin/encore production
```