<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180925095621 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $users_table = $schema->createTable('users');
        $users_table->addColumn('id', 'integer', ['autoincrement'=>true]);
        $users_table->addColumn('password', 'string', ['length' => 64]);
        $users_table->addColumn('username', 'string', ['length' => 254]);
        $users_table->addColumn('roles', 'text');
        $users_table->setPrimaryKey(array("id"));
        $users_table->addUniqueIndex(array("username"));

        $adverts_table = $schema->createTable('advertisements');
        $adverts_table->addColumn('id', 'integer', ['autoincrement'=>true]);
        $adverts_table->addColumn("user_id", "integer", ["notnull" => false]);
        $adverts_table->addColumn('title', 'string', ['length' => 255]);
        $adverts_table->addColumn('description', 'text');
        
        $adverts_table->addColumn(
            'created_at',
            'datetime',
            [
                'columnDefinition' => 'timestamp default current_timestamp',
            ]
        );
        $adverts_table->addForeignKeyConstraint($users_table, ["user_id"], ["id"], ["onUpdate" => "CASCADE"]);
        $adverts_table->setPrimaryKey(['id']);
    }

    public function down(Schema $schema) : void
    {
        $schema->dropTable('users');
        $schema->dropTable('advertisements');
    }
}
