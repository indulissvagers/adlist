<?php

namespace App\Repository;

use App\Entity\Advertisement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\User;

/**
 * @method Advertisement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Advertisement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Advertisement[]    findAll()
 * @method Advertisement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdvertisementRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Advertisement::class);
    }

    public function queryForAllAdvertisements()
    {
        return $this
            ->createQueryBuilder('advertisements')
            ->orderBy('advertisements.id', 'DESC')
            ->getQuery();
    }

    public function queryForUserAdvertisements(User $user)
    {
        return $this
            ->createQueryBuilder('advertisements')
            ->where('advertisements.user = ' . $user->getId())
            ->orderBy('advertisements.id', 'DESC')
            ->getQuery();
    }
}
