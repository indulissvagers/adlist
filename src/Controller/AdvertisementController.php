<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Form\UserType;
use App\Form\AdvertisementType;
use App\Entity\User;
use App\Entity\Advertisement;

class AdvertisementController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $advertisementsRepository = $em->getRepository(Advertisement::class);
        
        $paginator = $this->get('knp_paginator');
        
        $advertisements = $paginator->paginate(
            $advertisementsRepository->queryForAllAdvertisements(),
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('advertisement/list.html.twig', [
            'advertisements' => $advertisements
        ]);
    }

    /**
     * @Route("/user/advertisements", name="user_advertisements")
     */
    public function userAdvertisements(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $advertisementsRepository = $em->getRepository(Advertisement::class);

        $paginator = $this->get('knp_paginator');
        
        $advertisements = $paginator->paginate(
            $advertisementsRepository->queryForUserAdvertisements($user),
            $request->query->getInt('page', 1),
            10
        );
        
        return $this->render('advertisement/list.html.twig', [
            'advertisements' => $advertisements
        ]);
    }

    /**
     * @Route("/create", name="create_advertisement")
     */
    public function createAdvertisement(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $advertisement = new Advertisement();

        $form = $this->createForm(AdvertisementType::class, $advertisement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $advertisement = $form->getData();
            $advertisement->setUser($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($advertisement);
            $entityManager->flush();


            return $this->redirectToRoute('user_advertisements');
        }

        return $this->render('advertisement/create.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
